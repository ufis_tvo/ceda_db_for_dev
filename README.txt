This is the repository for changes of database in developer server. 

The sql script name should be in this format 

<table_name>.sql      (changes in data structure)
<table_name>_data.sql (insert, update, delete query)
  
Example: add column KEYF to table SYSTAB, and update KEYF data 

SYSTAB.sql
SYSTAB_data.sql



