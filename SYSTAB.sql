/*
Table SYSTAB 

20160224: UFIS-12787 add new field KEYF in table SYSTAB and update key field for 
                     table ACRTAB,ACTTAB,ALTTAB,APTTAB,NATTAB,CCATAB
*/


/*--- 20160224: UFIS-12787 -----------------------------------------------------*/

Alter table SYSTAB add KEYF char(1) default ' ';
COMMENT ON COLUMN "SYSTAB"."KEYF" IS 'Key Fields in Data Change (UFIS-12787)';

Alter table H00TAB modify KEYF char(64) default ' ';
Alter table H01TAB modify KEYF char(64) default ' ';
Alter table H02TAB modify KEYF char(64) default ' ';

commit;

/*------------------------------------------------------------------------------*/