/* 
20160226: fix UFIS-12787, update SYSTAB.KEYF after adding field KEYF tp SYSTAB 

*/

/*--- 20160226: UFIS-12787 --------------------------------------------------------------*/

/* KEYF for ACRTAB */
update SYSTAB set KEYF = '1' where TANA = 'ACR' and FINA = 'ACT3';
update SYSTAB set KEYF = '2' where TANA = 'ACR' and FINA = 'ACT5';
update SYSTAB set KEYF = '3' where TANA = 'ACR' and FINA = 'REGN';

/* KEYF for ACTTAB */
update SYSTAB set KEYF = '1' where TANA = 'ACT' and FINA = 'ACT3';
update SYSTAB set KEYF = '2' where TANA = 'ACT' and FINA = 'ACT5';


/* KEYF for ALTTAB */
update SYSTAB set KEYF = '1' where TANA = 'ALT' and FINA = 'ALC2';
update SYSTAB set KEYF = '2' where TANA = 'ALT' and FINA = 'ALC3';


/* KEYF for APTTAB */
update SYSTAB set KEYF = '1' where TANA = 'APT' and FINA = 'APC3';
update SYSTAB set KEYF = '2' where TANA = 'APT' and FINA = 'APC4';

/* KEYF for NATTAB */
update SYSTAB set KEYF = '1' where TANA = 'NAT' and FINA = 'TTYP';
update SYSTAB set KEYF = '2' where TANA = 'NAT' and FINA = 'TNAM';


/* KEYF for CCATAB */
update SYSTAB set KEYF = '1' where TANA = 'CCA' and FINA = 'CKIC';
update SYSTAB set KEYF = '2' where TANA = 'CCA' and FINA = 'CKBS';
update SYSTAB set KEYF = '3' where TANA = 'CCA' and FINA = 'CKES';

commit;

/*------------------------------------------------------------------------------------------*/